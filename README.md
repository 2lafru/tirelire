# tirelire

Web E2EE shared budget manager

## Install

For now, no configuration option is available. To start listening on port 8000, clone this repository and execute:

```bash
go run .
```

## Features

- End-to-end encryption and authentication using https://github.com/dchest/tweetnacl-js
- Progressive Web App: no need to have an Internet connection at all time!
- No account needed, sharing via URL or QR Code

## Start Hacking

tirelire uses some dependencies that are shipped with tirelire's source code.
While hacking on tirelire, you might want to check these dependencies documentation:
  - [PaperCSS](https://www.getpapercss.com/), the CSS framework
  - [idb](https://github.com/jakearchibald/idb#readme), a wrapper to access IndexedDB, an API of your browser
  - [QRCode.js](https://github.com/davidshimjs/qrcodejs#readme), a library to generate QR codes inside your browser
  - [tweetnacl-js](https://github.com/dchest/tweetnacl-js) and [tweetnacl-util-js](https://github.com/dchest/tweetnacl-util-js) to handle the crypto
