// version = 3
const CACHE_NAME = 'tirelire'

const getPath = url => '/' + url.split('/').slice(3).join('/')
const router = async (req) => {
  // Cached resources
  const path = getPath(req.url)

  const cacheRes = await caches.match(path)
  if (cacheRes) {
    return cacheRes
  }

  if (/\/refresh$/.test(path)) {
    console.log('Updating application')
    await populateCache()
    return new Response('OK')
  }

  return fetch(req)
}

const populateCache = async () => {
  const cache = await caches.open(CACHE_NAME)
  const b = getPath(self.registration.scope)

  return cache.addAll([
    `${b}`,
    `${b}manifest.webmanifest`,
    `${b}favicon.ico`,
    `${b}favicon.png`,
    `${b}index.html`,

    `${b}css/paper.min.css`,
    `${b}css/custom.css`,
    `${b}css/fonts.css`,

    `${b}fonts/neucha.woff`,
    `${b}fonts/neucha.woff2`,
    `${b}fonts/patrick.woff`,
    `${b}fonts/patrick.woff2`,

    `${b}js/config.js`,
    `${b}js/display.js`,
    `${b}js/form-expense.js`,
    `${b}js/idb.min.js`,
    `${b}js/index.js`,
    `${b}js/qrcode.min.js`,
    `${b}js/nacl-fast.min.js`,
    `${b}js/nacl-util.min.js`,
    `${b}js/settle.js`,
    `${b}js/store.js`,
  ])
}

self.addEventListener('install', (e) => {
  console.log('Installing service worker')
  e.waitUntil(populateCache()) // waitUntil used to prevent SW termination
})

self.addEventListener('fetch', e => e.respondWith(router(e.request)))
