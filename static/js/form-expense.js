import { clearChildren, displayEntries } from './display.js'
import Store from './store.js'

class FormExpense {
  constructor(store) {
    this.store = store

    this.desc = $('#modal-expense-description')
    this.amount = $('#modal-expense-amount')
    this.user = $('#modal-expense-user')
    this.list = $('#modal-expense-user-list')
    this.listNew = $('#modal-expense-user-list-new')
    this.listAdd = $('#modal-expense-user-list-add')
    this.newUser = $('#modal-expense-new-user')
    this.newUserG = $('#modal-expense-new-user-group')
    this.submitBtn = $('#modal-expense-submit')
    this.deleteBtn = $('#modal-expense-delete')

    this.user.onchange = ({ target }) => {
      this.newUserG.classList.add('hidden')

      if (target.value === 'new') {
        this.newUserG.classList.remove('hidden')
        this.newUser.value = ''
        this.newUser.focus()
      }
    }

    this.newUser.onblur = ({ target }) => {
      if (target.value) {
        this.addUserInList(target.value)
      }
    }

    this.listAdd.onclick = () => {
      if (this.listNew.value) {
        this.addUserInList(this.listNew.value)
        this.listNew.value = ''
        this.listNew.focus()
      }
    }

    this.listNew.onkeyup = ({ keyCode }) => {
      if (keyCode === 13) {
        this.listAdd.click()
      }
    }

    this.submitBtn.onclick = () => this.submit()
    this.deleteBtn.onclick = () => this.delete()
  }

  async submit() {
    const user = this.user.value === "new" ? this.newUser.value : this.user.value
    if (!user) {
      return // TODO give feedback
    }

    const paidFor = [...new Set($$('input[name=modal-expense-user-list-checkboxes]:checked').map(e => e.value))]
    if (paidFor.length === 0) {
      return // TODO give feedback
    }

    const entry = {
      uuid: this.uuid || Store.uuid(),
      description: this.desc.value || '(none)',
      amount: +this.amount.value,
      user,
      paidFor,
    }

    await this.store.putEntry(this.ledger, entry)
    await displayEntries(this.store, this.ledger)
    $('#modal-expense').checked = false
  }

  async delete() {
    await this.store.rmEntry(this.ledger, this.uuid)
    await displayEntries(this.store, this.ledger)
    $('#modal-expense').checked = false
  }

  populate(entry) {
    this.uuid = entry.uuid
    this.deleteBtn.classList.remove('hidden')
    this.desc.value = entry.description
    this.amount.value = entry.amount
    this.user.value = entry.user
    $$('input[name=modal-expense-user-list-checkboxes]').forEach((e) => {
      e.checked = entry.paidFor.indexOf(e.value) >= 0
    })
  }

  async open(params) {
    const { focus } = params || {}
    if (focus) {
      setTimeout(() => this.desc.focus(), 100)
    }

    this.uuid = undefined
    this.deleteBtn.classList.add('hidden')
    this.ledger = $('.ledger:checked').value
    const knownUsers = Store.listUsers(await this.store.listEntries(this.ledger))

    this.desc.value = ''
    this.amount.value = ''
    clearChildren(this.user)
    clearChildren(this.list)

    const legend = document.createElement('legend')
    legend.textContent = 'Paid for'
    this.list.appendChild(legend)

    knownUsers.forEach((u) => {
      const option = document.createElement('option')
      option.setAttribute('value', u)
      option.textContent = u
      this.user.appendChild(option)
      this.addUserInList(u)
    })

    const option = document.createElement('option')
    option.setAttribute('value', 'new')
    option.textContent = 'New'
    this.user.appendChild(option)
    this.user.onchange({ target: { value: knownUsers.length === 0 ? 'new' : knownUsers[0] } })

    $('#modal-expense').checked = true
  }

  addUserInList(user) {
    const i = this.list.childElementCount
    const userListID = `modal-expense-user-list-${i}`
    const label = document.createElement('label')
    label.setAttribute('for', userListID)
    label.setAttribute('class', 'paper-check')

    const input = document.createElement('input')
    input.setAttribute('id', userListID)
    input.setAttribute('type', 'checkbox')
    input.setAttribute('name', 'modal-expense-user-list-checkboxes')
    input.setAttribute('checked', 'true')
    input.value = user

    const span = document.createElement('span')
    span.textContent = user

    label.appendChild(input)
    label.appendChild(span)
    this.list.appendChild(label)
  }
}

export default FormExpense
