import Store from './store.js'

window.$ = (...args) => document.querySelector(...args)
window.$$ = (...args) => Array.from(document.querySelectorAll(...args))

const events = {}

const currentLedger = () => {
  return $('.ledger:checked').value
}

const clearChildren = (container) => {
  while (container.firstChild) {
    container.removeChild(container.firstChild)
  }
}

const displayLedgers = async (store) => {
  const ledgers = await store.listLedgers()
  const container = $('#ledgers-list')
  clearChildren(container)

  // Populate ledger list
  ledgers.push({
    public: 'new',
    name: 'New',
  })
  ledgers.forEach((l) => {
    const input = document.createElement('input')
    input.setAttribute('class', 'ledger')
    input.setAttribute('value', l.public)
    input.setAttribute('id', `tab-${l.public}`)
    input.setAttribute('type', 'radio')
    input.setAttribute('name', 'ledgers')
    input.onclick = async ({ target }) => {
      await displayEntries(store, target.value)
      if (!target.synchronized) {
        $('#sync').click()
        target.synchronized = true
      }
    }

    const label = document.createElement('label')
    label.setAttribute('for', `tab-${l.public}`)
    label.setAttribute('class', 'ledger-tab')
    label.textContent = l.name

    container.appendChild(input)
    container.appendChild(label)
  })
}

const displayEntries = async (store, ledger) => {
  if (!ledger) {
    ledger = currentLedger()
  }

  $('#view-report').classList.add('hidden')
  $('#view-misc').classList.add('hidden')
  $('#view-main-table').classList.add('hidden')
  $('#view-new-ledger').classList.add('hidden')
  $('#sync').setAttribute('class', 'btn-small')

  if (ledger === 'new') {
    $('#view-new-ledger').classList.remove('hidden')
    $('#ledger-name').focus()
    return
  }

  const entries = await store.listEntries(ledger)
  const container = $('#entries-list')
  clearChildren(container)

  if (entries.length === 0) {
    const row = document.createElement('tr')
    const cell = document.createElement('td')
    cell.setAttribute('colspan', '3')
    cell.textContent = 'No operation registered! Start by clicking on "Add expense".'

    row.appendChild(cell)
    container.appendChild(row)
  }

  entries.reverse()
  entries.forEach((e) => {
    if (e.deleted) {
      return
    }

    const row = document.createElement('tr')
    row.onclick = () => events.onEdit(e)
    const desc = document.createElement('td')
    desc.textContent = e.description
    const user = document.createElement('td')
    user.textContent = e.user
    user.setAttribute('class', 'sm-hidden')
    const amount = document.createElement('td')
    amount.textContent = e.amount

    row.appendChild(desc)
    row.appendChild(user)
    row.appendChild(amount)
    container.appendChild(row)
  })

  $('#view-main-table').classList.remove('hidden')
}

const toggleReport = () => {
  $('#view-main-table').classList.toggle('hidden')
  $('#view-report').classList.toggle('hidden')
}

const toggleMisc = async (store) => {
  $('#view-main-table').classList.toggle('hidden')
  $('#view-misc').classList.toggle('hidden')
  if (!store) {
    return
  }

  const ledger = currentLedger()
  const code = await store.exportLedger(ledger)
  const url = `${window.location.origin}${window.location.pathname}#${code}`

  $('#export-url').textContent = url
  const qrEl = $('#export-qrcode')
  clearChildren(qrEl)
  new QRCode(qrEl, {
    text: url,
	  width: 256,
	  height: 256,
	  colorDark : "#000000",
	  colorLight : "#ffffff",
	  correctLevel : QRCode.CorrectLevel.M
  })
}

const openReport = async (store) => {
  const reports = $('#reports')
  clearChildren(reports)
  const ledger = currentLedger()
  const { settlement, totals } = await store.report(ledger)

  const title1 = document.createElement('h4')
  title1.textContent = 'Settlement'
  reports.appendChild(title1)

  if (settlement.length === 0) {
    const p = document.createElement('p')
    p.textContent = 'Accounts are balanced, well done!'
    reports.appendChild(p)
  }

  settlement.forEach(({ from, to, amount }) => {
    const p = document.createElement('p')
    p.innerHTML = `<strong>${from}</strong> must send <span class="badge secondary">${amount}</span> to <strong>${to}</strong>`
    reports.appendChild(p)
  })

  const title2 = document.createElement('h4')
  title2.textContent = 'Totals'
  reports.appendChild(title2)

  if (Object.keys(totals).length === 0) {
    const p = document.createElement('p')
    p.textContent = 'No registered operation yet.'
    reports.appendChild(p)
  }

  for (const user in totals) {
    const p = document.createElement('p')
    p.innerHTML = `<strong>${user}</strong> spent a total of <span class="badge primary">${totals[user]}</span>`
    reports.appendChild(p)
  }

  toggleReport()
}

export {
  events,
  currentLedger,
  clearChildren,
  displayLedgers,
  displayEntries,
  toggleReport,
  toggleMisc,
  openReport,
}
